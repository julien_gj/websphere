import AdminApplication
from org.apache.log4j import Logger

class Application:

	def __init__(self, appName, nodeName, servName, path=""):
		self.appName = appName
		self.nodeName = nodeName
		self.servName = servName
		self.path = path

	def getAppStatus(self):

		self.nodeName = raw_input("\nSpecify the name of the node where the application is installed\n")
		self.servName = raw_input("\nSpecify the name of the server where the application is installed\n")
		try:
			if AdminApplication.AdminControl.completeObjectName('type=Application,name=' + self.appName + ',*'):
				print "The application is up and running."
			else:
				print "The application is down."
		except:
			print "There is no application named ", self.appName

	def installApp(self):

		if not AdminApplication.checkIfAppExists(self.appName):
			self.appName = raw_input("\nSpecify the name of the application you wish to install\n")
			self.path = raw_input("\nSpecify the path of the application you wish to install\n")
			self.nodeName = raw_input("\nSpecify the name of the node where you wish to install the application\n")
			self.servName = raw_input("\nSpecify the name of the server where you wish to install the application\n")
			AdminApplication.installAppWithNodeAndServerOptions(self.appName, self.path, self.nodeName, self.servName)

			# AdminApplication.installWarFile(appName, warFile, nodeName, serverName, contextRoot, failonerror=AdminUtilities._BLANK_)
			# AdminApp.install('location_of_ear.ear','[-node nodeName -cell cellName -server servName]')
			# AdminApplication.install('C:/fakepath/LibraryWeb.war', '[ -nopreCompileJSPs -distributeApp -nouseMetaDataFromBinary -nodeployejb -appname LibraryWeb_war -createMBeansForResources -noreloadEnabled -nodeployws -validateinstall warn -noprocessEmbeddedConfig -filepermission .*\.dll=755#.*\.so=755#.*\.a=755#.*\.sl=755 -noallowDispatchRemoteInclude -noallowServiceRemoteInclude -asyncRequestDispatchType DISABLED -nouseAutoLink -contextroot /LibraryURL  -MapResRefToEJB [[ UniLibraryWeb "" LibraryWeb.war,WEB-INF/web.xml jdbc/UniLibraryDS javax.sql.DataSource CleJNDILibraryDataSource "" "" "" ]] -MapModulesToServers [[ UniLibraryWeb LibraryWeb.war,WEB-INF/web.xml WebSphere:cell=Cell1,node=NodeBCell1,server=server1+WebSphere:cell=Cell1,node=NodeBCell1,server=server2+WebSphere:cell=Cell1,node=NodeBCell1,server=server10 ]] -MapWebModToVH [[ UniLibraryWeb LibraryWeb.war,WEB-INF/web.xml default_host ]] -CtxRootForWebMod [[ UniLibraryWeb LibraryWeb.war,WEB-INF/web.xml /LibraryURL ]]]')

			Logger.logger.info(self.appName + " has successfully been created!")
		else:
			print "The application has already been installed!"

	def uninstallApp(self):
		self.appName = raw_input("\nSpecify the name of the application you wish to uninstall\n")

		if AdminApplication.checkIfAppExists(self.appName):
			AdminApplication.uninstallApplication(self.appName)
			Logger.logger.info(self.appName + " has successfully been deleted!")
		else:
			print "There is no application named ", self.appName

	def startApp(self):
		self.appName = raw_input("\nSpecify the name of the application you wish to start\n")
		self.nodeName = raw_input("\nSpecify the name of the node where you wish to start the application\n")
		self.servName = raw_input("\nSpecify the name of the server where you wish to start the application\n")
		if AdminApplication.checkIfAppExists(self.appName):
			AdminApplication.startApplicationOnSingleServer(self.appName, self.nodeName, self.servName)
			Logger.logger.info(self.appName + " has successfully been started!")
		else:
			print "There is no application named ", self.appName

	def stopApp(self):
		self.appName = raw_input("\nSpecify the name of the application you wish to stop\n")
		self.nodeName = raw_input("\nSpecify the name of the node where you wish to stop the application\n")
		self.servName = raw_input("\nSpecify the name of the server where you wish to stop the application\n")
		if AdminApplication.checkIfAppExists(self.appName):
			AdminApplication.stopApplicationOnSingleServer(self.appName, self.nodeName, self.servName)
			Logger.logger.info(self.appName + " has successfully been stopped!")
		else:
			print "There is no application named ", self.appName

	def status(self):
		self.appName = raw_input("\nSpecify the name of the application you wish to see the status of\n")
		if AdminApplication.checkIfAppExists(self.appName):
			Application.getAppStatus()
		else:
			print "There is no application named ", self.appName