# -*-coding:iso-8859-1 -*-
#Creation d'un programme interactif qui permettra l'administration de serveurs.
#L'utilisateur aura la possibilité de démarrer, arrêter et lister les serveurs sur WebSphere.
#L'admin aura la possibilité de créer, supprimer des serveurs et gérer la liste de serveurs sur WebSphere

import AdminServerManagement
from org.apache.log4j import PropertyConfigurator, Logger

PropertyConfigurator.configure("C:\\Users\\ib\\Documents\\GestionServeur\\logs\\log4j.properties")


#Classe serveur qui décrit un objet serveur
class Server:

    def __init__(self, nom, node):
        self.nom = nom
        self.node = node
        self.status = "off"
    
    def demarrer(self):
        if self.status == "off":
            self.status = "on"
        else:
            raise ServException("Le serveur est déjà démarré.")

    def arreter(self):
        if self.status == "on":
            self.status == "off"
        else:
            raise ServException("Le serveur est déjà arrêté.")

    def afficher(self):
        print '|', self.nom, "Noeud :", self.node, "Statut :", self.status,'|'
        
    
    def statutServeurIn(self):
        try:
            toto = AdminServerManagement.getServerPID(self.node, self.nom)
            logger.info("Le serveur est demarré")
            stat = "on"
        except:
            logger.info("Le serveur est arrêté.")
            stat = "off"
        return stat
   

#Classe listServeurs qui regroupe la liste des serveurs présents dans le WebSphere de l'utilisateur
class listeServeurs:

    def  __init__(self):
        self.donnees = {}
    
    def add_server(self, myServ):
        if myServ is not None :
            cle = myServ.nom
            self.donnees[cle] = myServ
        else:
            raise ServException("Le serveur existe déjà.")
    
    def del_server(self, myServ):
        if myServ is not None :
            cle = myServ.nom
            del self.donnees[cle] 
        else:
            raise ServException("Le serveur est déjà supprimé.")
    
    def afficherListe(self):
        if self.donnees is not None :
            for cle in self.donnees.keys():
                print 'la cle : ',cle,self.donnees[cle].afficher()
        else:
            raise ServException("Il n'y a pas de serveur à afficher.")

    def importerServList(self):

        serveurs_chaine = AdminTask.listServers('[-serverType APPLICATION_SERVER]')
        if serveurs_chaine is not None:
            serveurs_liste = serveurs_chaine.split("\n")
            for serveur_chaine in serveurs_liste:
                nom_serveur = serveur_chaine[0:serveur_chaine.index("(")]
                elements = serveur_chaine.split("/")
                nom_noeud = elements[3]
                myServ = Server(nom_serveur, nom_noeud)
                myServ.status = myServ.statutServeurIn()
                myListServ.add_server(myServ)
        else:
            raise ServException("La liste des serveurs existants est vide, c'est peut-être qu'il n'y a pas de serveur existant.")



#classe de gestion des exceptions
class ServException(Exception):
    def __init__(self, message):
        self.message = message


# procedure
logger = Logger.getLogger("App")

logger.info("      ####################################################################")
logger.info("      ###          Bienvenue dans le script de COCO qui tue            ###")
logger.info("      ####################################################################")
logger.info("\n")

# creation des objets que l'on va utiliser dans le corps du programme
continuer = "oui"
#Initialisation du dictionnaire pour stoker la liste de serveur
myListServ = listeServeurs()
# boucle principale

while continuer == 'oui' :  
    logger.info("\n")      
    logger.info("                ################################################")
    logger.info("                ##   0 - Gestion des Serveurs                 ##")
    logger.info("                ##   1 - Gestion des Applications             ##")
    logger.info("                ##   2 - Gestion des Noeuds                   ##")
    logger.info("                ##   3 - Sortie du script                     ##")
    logger.info("                ################################################")
    logger.info("\n")

    reponse = int(raw_input("Veuillez entrer le numéro correspondant à l'action voulue : "))


    if reponse == 0 :
# boucle serveur
# creation d'objet servant dans la boucle serveur
        retourServer = "oui"
        logger.info("\n")
        logger.info("      ####################################################################")
        logger.info("      ###        Bienvenue dans la partie gestion de serveur           ###")
        logger.info("      ####################################################################")
        logger.info("\n")


        while retourServer == 'oui':

            logger.info("\n")
            logger.info("                ################################################")
            logger.info("                ##   0 - Import de la liste des serveurs      ##")
            logger.info("                ##   1 - Affichage de la liste des serveurs   ##")
            logger.info("                ##   2 - Création d'un serveur                ##")
            logger.info("                ##   3 - Suppression d'un serveur             ##")
            logger.info("                ##   4 - Démarrer serveur                     ##")
            logger.info("                ##   5 - Arrêter serveur                      ##")
            logger.info("                ##   6 - Statut de serveur                    ##")
            logger.info("                ##   7 - Retour au menu principal             ##")
            logger.info("                ################################################")

            raiponce = int(raw_input("Veuillez entrer le numéro correspondant à l'action voulue : "))

            if raiponce == 0:
                try:
                    myListServ.importerServList()
                except ServException, e:
                    logger.info(e.message)

            elif raiponce == 1:
                try:
                    myListServ.afficherListe()
                except ServException, e:
                    logger.warning(e.message)

            elif raiponce == 2:
                try:
                    nom=str(raw_input("Quel sera le nom du serveur ? "))
                    node=str(raw_input("Quel est le nom de son node ? "))
                    AdminTask.createApplicationServer(node, '[-name '+nom+' -templateName default -genUniquePorts true ]')
                    AdminConfig.save()
                except ServException, e:
                    logger.error(e.message)

            elif raiponce == 3:
                try:
                    nom = str(raw_input("Quel sera le nom du serveur ? "))
                    node = str(raw_input("Quel est le nom de son node ? "))
                    del myListServ.donnees[nom]
                    AdminTask.deleteServer('[-serverName '+nom+' -nodeName '+node+' ]')
                    AdminConfig.save()
                except ServException, e:
                    logger.warning(e.message)

            elif raiponce == 4:
                try:
                    nom = str(raw_input("Quel sera le nom du serveur ? "))
                    node = str(raw_input("Quel est le nom de son node ? "))
                    AdminControl.invoke('WebSphere:name=NodeAgent,process=nodeagent,platform=common,node='+node+',diagnosticProvider=true,version=7.0.0.11,type=NodeAgent,mbeanIdentifier=NodeAgent,cell=WINIB-NKGGQ1KTTCell01,spec=1.0', 'launchProcess', '['+nom+']', '[java.lang.String]')
                    AdminConfig.save()
                except ServException, e:
                    logger.error(e.message)

            elif raiponce == 5:
                try:
                    nom = str(raw_input("Quel sera le nom du serveur ? "))
                    node = str(raw_input("Quel est le nom de son node ? "))
                    AdminServerManagement.stopSingleServer(node, nom)
                    AdminConfig.save()
                except ServException, e:
                    logger.error(e.message)

            elif raiponce == 6:
                try:
                    nom = str(raw_input("Quel sera le nom du serveur ? "))
                    node = str(raw_input("Quel est le nom de son node ? "))
                    AdminServerManagement.getServerPID(node, nom)
                    logger.info("Le serveur est demarré")
                except:
                    logger.info("Le serveur est arrêté.")         
                
                AdminConfig.save()

            elif raiponce == 7:
                logger.info("Menu principal incoming")
                break

            else:
                logger.info("Evidemment il fallait taper un chiffre entre 0 et 7")
# boucle App
    if reponse == 1 :

# creation d'objet servant dans la boucle App
        retourApp = "oui"

        while retourApp == 'oui':
            logger.info("                ################################################")
            logger.info("                ##   0 - Import de la liste des serveurs      ##")
            logger.info("                ##   0 - Affichage de la liste des serveurs   ##")
            logger.info("                ##   0 - Création d'un serveur                ##")
            logger.info("                ##   0 - Suppression d'un serveur             ##")
            logger.info("                ##   0 - Démarrer serveur                     ##")
            logger.info("                ##   0 - Arrêter serveur                      ##")
            logger.info("                ##   0 - Statut du serveur                    ##")
            logger.info("                ##   0 - Retour au menu principal             ##")
            logger.info("                ################################################")

            repApp = int(raw_input("Veuillez entrer le numéro correspondant à l'action voulue : "))
            if repApp == 0 :
                break
    if reponse == 2 :
        logger.info("/_TODO_\ Le travaille reste à faire !! /_TODO_")
        
    if reponse == 3 :
        break
    logger.info("\n")
    continuer = str(raw_input("Veuillez taper 'oui' pour continuer, 'non' pour arrêter : "))
    logger.info("\n")

logger.info("\n")
logger.info("######################################################################")
logger.info("##                            The end                               ##")
logger.info("######################################################################")


