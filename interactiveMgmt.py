# coding: iso8859_1
import serverUtils




def nodeGestion():
    try:
        rep = 'null'
        print "*****GESTION DES NOEUDS*****" 
        while  rep != 0 :
            rep = int(raw_input("Arreter un Noeud    :1\nDemarrer un Noeud   :2\nSupprimer un Noeud  :3\nFin des operations :0\n > "))
            if rep == 1:
                stopNode()
            elif rep == 2:
                startNode()
            elif rep == 3:
                deleteNode()
            elif rep == 0:
                print "Fin de l'operation sur les noeuds\n\n" 
                operations()
                break
    except:
        nodeGestion()

def appGestion():
    try:
        rep = "null"
        print "*** GESTION DES APPLICATIONS ***" 
        while  rep != 0 :
            rep = int(raw_input("Ajouter une Application    :1\nSupprimer une Application  :2\nTerminer les operations   :0\n > "))
            if rep == 1:
                AddApp()
            elif rep == 2:
                DeleteApp()
            elif rep == 0 :
                print "Fin de l'operation sur les applications\n\n" 
                operations()
    except:
        appGestion()

def serversGestion():
    try:
        rep = 'null'
        print "****GESTION DES SERVEURS****" 
        
        while  rep != 0 :
            rep = int(raw_input("Creer un Serveur      :1\nSupprimer un Serveur  :2\nDemarrer un serveur   :3\nAfficher les serveurs :4\n Arreter un serveur   :5 \n Fin des operations   :0\n\n  > "))
            if rep == 1:
                myCell.creerServeur()                #ça marche
            elif rep == 2:
                myCell.supprimerServeur()
            elif rep == 3:
                myCell.demarrerServeur()
            elif rep == 4:
                myCell.afficherServeurs()
            elif rep == 5 :
                myCell.arreterServeur()
            elif rep == 0:
                print "Fin de l'operation sur les serveurs\n\n" 
                operations()
    except:
        serversGestion()

def operations():
    try:
        rep = 'null'
        while  rep != 0 :
            print "******  OPERATIONS ******" 
            rep = int(raw_input("Gestion des noeuds       :1\nGestion des serveurs     :2\nGestion des applications :3\nFin des operations      :0\n > "))
            if rep == 1:
                print "gestion des noeuds"
                nodeGestion()
            elif rep == 2:
                serversGestion()
            elif rep == 3:
                appGestion()
            elif rep == 0:
                print "Fin des operations\n\n" 
                break
    except Exception, e:
        print e,"erreur"
        operations()

myCell= serverUtils.Cellule()
myCell.synchroniser()
operations()