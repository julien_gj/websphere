# -*-coding:iso-8859-1 -*-
# gestion des clusters
import AdminClusterManagement
import time

indexClusters={}        # un tableau associatif, variable globale
statutEnFr = {'stopped' : ' Arrete ' , 'partial' : ' Partiellement demarre ' , 'running' : ' Completement demarre '}
def creerGrappe():
    try :
        nouveau = raw_input(' Nom du cluster a creer \n >>> ')
        #arguments = '[-clusterConfig [-clusterName %s -preferLocal true] -replicationDomain [-createDomain true]]'%(nouveau)
        AdminConfig.list('ServerCluster', AdminConfig.getid( '/Cell:Cellule1/'))
        AdminTask.createCluster(['-clusterConfig', ['-clusterName', nouveau, '-clusterType', 'APPLICATION_SERVER']])
        AdminConfig.save()
        #AdminTask.createCluster(arguments)
        print ' Souhaitez - vous creer des serveurs sur ce cluster ? O/n'
        choix = raw_input('>>> ')
        if choix[0].lower() == 'n' :
            pass
        else :
                name = raw_input('Entrez le nom du premier serveur \n>>> ')
                node = raw_input('Entrez le nom du noeud \n>>> ')
                AdminTask.createClusterMember(['-clusterName', nouveau, '-memberConfig', ['-memberNode', node, '-memberName', name, '-memberWeight', '2', '-genUniquePorts', 'true', '-replicatorEntry', 'true'], '-firstMember', ['-templateName', 'default', '-nodeGroup', 'DefaultNodeGroup', '-coreGroup', 'DefaultCoreGroup']])
                #AdminConfig.save()
                choix2 = raw_input(' Souhaitez-vous ajouter un serveur ? (O/n) \n>>> ')
                while choix2[0].lower() != 'n' :
                    name = raw_input('Entrez le nom du nouveau serveur \n>>> ')
                    node = raw_input('Entrez le nom du noeud \n>>> ')
                    AdminTask.createClusterMember(['-clusterName', nouveau, '-memberConfig', ['-memberNode', node, '-memberName', name, '-memberWeight', '2', '-genUniquePorts', 'true', '-replicatorEntry', 'true']])
                    #AdminConfig.save()
                    choix2 = (raw_input(' Souhaitez-vous ajouter un serveur ? (O/n) \n>>> '))
        AdminConfig.save()
        inventaireClusters()
        afficherClusters()
    except :
        creerGrappe()

    #AdminTask.createClusterMember('[-clusterName Grappe2 -memberConfig [-memberNode NodeBCellule1 -memberName Serveur5 -memberWeight 2 -genUniquePorts true -replicatorEntry true] -firstMember [-templateName default -nodeGroup DefaultNodeGroup -coreGroup DefaultCoreGroup]]')

def supprimerGrappe() :
    afficherClusters()
    try :
        clusterName = raw_input(' Nom du cluster a supprimer \n >>> ')
        indexClusters[clusterName].arreter()
        AdminTask.deleteCluster(['-clusterName', clusterName])
        AdminConfig.save()
        while clusterName in indexClusters.keys() :
            print '... wait 10s'
            time.sleep(10)
            del indexClusters[clusterName]
            inventaireClusters()
        print ' INFO Cluster %s deleted ' %(clusterName)
    except :
        supprimerGrappe()

    
def inventaireClusters() :
    print '\nRecuperation de la liste des clusters\n\n####################################'
    liste=AdminClusterManagement.listClusters() #récupératon d'une liste
    for string in liste :
        nomGrappe = string.split('(')[0]    #récupération du nom du cluster
        nomCellule = string.split('/')[1]    # recuperation du nom de la cellule ou il creche
        grappe = Cluster(nomGrappe)           # instanciation d'un objet Cluster()
        grappe.cellule = nomCellule
        grappe.statut = clusterStatus(nomGrappe,nomCellule) #champ statut 
        indexClusters[nomGrappe]=grappe       #remplissage du tableau associatif
        #print grappe.nom, grappe.statut
        listeServeurs = AdminClusterManagement.listClusterMembers(nomGrappe)
        # for string in listeServeurs :
            # print string.split('(')[0]
    print '\n####################################\n\n'
            
def serverStatus(name,node,cell):
    good=999
    try :
        server = AdminControl.completeObjectName('cell='+cell+',node='+node+',name='+name+',type=Server,*')
        state = AdminControl.getAttribute(server, 'state')
        print "The server", name, "is", state
        #logger.info(name + " is up and running!")
        good=0
        return good

    except:
        print "The server", name, "is STOPPED"
        #logger.info(name + " is currently down!")
        good=1
        return good
        
        
def clusterStatus(name,cell):
    good=999
    try :
        cluster = AdminControl.completeObjectName('cell='+cell+',name='+name+',type=Cluster,process=dmgr,*')
        state = AdminControl.getAttribute(cluster, 'state') # running, partial.start ou stopped
        # print "The cluster", name, "is", state
        #logger.info(name + " is up and running!")
        good=1
        return state

    except:
        print "The cluster", name, "is STOPPED"
        #logger.info(name + " is currently down!")
        good=0
        return good
        
def afficherClusters() :
    #inventaireClusters()
    print '--------------------\n Liste des Clusters \n-------------------- '
    for element in indexClusters.values() :
        print str(element)
    print '--------------------\n'

class Cluster :
    def __init__(self,nom):
        self.nom = nom
        self.statut = ""
        self.cellule=""
        self.listeServeurs={}

    def __str__(self):
        statutFr = statutEnFr[self.statut.split('.')[2]]
        return ' [ %s, %s ] '%(self.nom,statutFr)
    
    def demarrer(self):
        if clusterStatus(self.nom,self.cellule) !=  'websphere.cluster.running' :
            print "** INFO demarrage complet du cluster ! ** \n"
            AdminClusterManagement.startSingleCluster(self.nom)
            while clusterStatus(self.nom,self.cellule) != 'websphere.cluster.running' :
                print "... wait 10s"
                time.sleep(10) 
            print " INFO Cluster %s running " %(self.nom)#, clusterStatus('Grappe1','Cellule1')
            self.statut = 'websphere.cluster.running'
    
    def arreter(self):
        if clusterStatus(self.nom,self.cellule) !=  'websphere.cluster.stopped' :
            print "** INFO Arret complet du cluster ! ** \n "
            AdminClusterManagement.stopSingleCluster(self.nom)
            while clusterStatus(self.nom,self.cellule) != 'websphere.cluster.stopped' :
                print "... wait 10s"
                time.sleep(10) 
            print " INFO Cluster %s stopped " %(self.nom) #, clusterStatus('Grappe1','Cellule1')
            self.statut = 'websphere.cluster.stopped'


def arreterCluster() :
    afficherClusters()
    try : 
        nomDuCluster = raw_input("donner le nom du cluster a arreter \n >>>  ")
        indexClusters[nomDuCluster].arreter()
    except :
        arreterCluster()
def demarrerCluster() :
    afficherClusters()
    try :
        nomDuCluster = raw_input("donner le nom du cluster a demarrer \n >>>  ")
        indexClusters[nomDuCluster].demarrer()
    except :
        demarrerCluster()

    
def operationsClusters():
    inventaireClusters()
    #serverStatus('Serveur1','NodeBCellule1','Cellule1')
    #clusterStatus('Grappe1','Cellule1')
    input = 999
    while input != 0 :
        afficherClusters()
        print ' 1 : Arreter un cluster \n 2 : Demarrer un cluster \n 3 : Creer un cluster  \n 4 : Supprimer un cluster \n 0 : Terminer '
        input=int(raw_input('Operation a effectuer \n >>>  '))
        if input == 1 :
            arreterCluster()
        elif input == 2 :
            demarrerCluster()
        elif input == 3 :
            creerGrappe()
        elif input == 4 :
            supprimerGrappe()
        
    
if __name__ == '__main__':
    operationsClusters()
    
    
    #clusterStatus(nomDuCluster,'Cellule1')
    
#SCénario : afficher les clusters et leur statut