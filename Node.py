import os
import sys
import serverUtils
import cellUtils
from org.apache.log4j import PropertyConfigurator
from org.apache.log4j import Logger


PropertyConfigurator.configure("C:\\Users\\ib\\Documents\\GestionServeur\\websphere\\log4j.properties")
logger = Logger.getLogger("Interface_Web")

######## classe de gestion des exceptions
class ServException(Exception):
    def __init__(self, message):
        self.message = message


######## classe des nodes
class Node:

    def __init__(self, nom):
        self.nom = nom
        self.status="off"
    
    def demarrer(self,node):
        if self.status == "off":
            cmd="C:/IBM/WebSphere/AppServer/bin/startNode.bat -profileName %s -username websphere -password websphere8" %(node)
            os.system(cmd)
        else:
            raise ServException("Le node est déjà démarré.")
    
    def arreter(self,node):
        print(self.status)
        if self.status != "off":
            cmd="C:/IBM/WebSphere/AppServer/bin/stopNode.bat -profileName %s -username websphere -password websphere8" %(node)
            os.system(cmd)
        else:
            raise ServException("Le node est déjà arrêté.")

    def statut(self,node):    
        etat=AdminNodeManagement.isNodeRunning(node)
        print(etat)


######## classe de la liste des nodes

class listNodes:
    def __init__(self):
        self.nodeList=[]

    def creer(self):
        #### ! changer le hostname & login
        nom_node=raw_input("Quel sera le nom du node ? format NodeX : ")
        nom_node=nom_node.strip()
        nom_cell=raw_input("Quel sera le nom de la cellule ? format CellX : ")
        nom_cell=nom_cell.strip()
        nom_profile=nom_node+nom_cell

        # if nom_profile is not in self.nodeList:
        #     cmd="C:/IBM/WebSphere/AppServer/bin/manageprofiles.bat -create -templatePath C:/IBM/WebSphere/AppServer/profileTemplates/managed -profileName %s -profilePath C:/IBM/WebSphere/AppServer/profiles/%s -hostname julien -nodeName %s -cellName %s -dmgrHost julien -dmgrPort 8879 -dmgrAdminUserName websphere -dmgrAdminPassword websphere" %(nom_profile,nom_profile,nom_node,nom_cell)					
        #     os.system(cmd)
        #     newNode=Node(nom_profile)
        #     self.nodeList.append(newNode)
        #     print(self.nodeList)
        # else:
        #     raise ServException("Le node existe déjà.")
    
    # def supprimer(self):
        # if 
        # rmCell=cellUtils.delete_cell()
        # del nodeList[rmCell]
		# #if
        # cmd="C:/IBM/WebSphere/AppServer/bin/manageprofiles.bat -delete -profileName %s -username websphere -password websphere" %(ans[1])
		# os.system(cmd)
        #else:
        #   raise ServException("Ce node n'existe pas")

######## main

if __name__ == '__main__':

    cellUtils.getListServ()
    os.system("clear")

    retour = "oui"
    while retour == "oui":

        print("\n-----Gestion des nodes-----\n")
        print("0 - Afficher les noeuds")
        print("1 - Creer un noeud")
        print("2 - Supprimer un noeud")
        print("3 - Demarrer un noeud")
        print("4 - Arreter un noeud")
        print("5 - Consulter le statut d'un noeud")
        print("\n")

        myNode=Node("commetuveux")
        myList=listNodes()
        save_servers={}
        
        cellUtils.getListServ()
        rep = int(raw_input("Quelle action effectuer ? : "))

        if rep == 1:
            # try:
            myList.creer()
            # except:     

        elif rep == 2:
            try:
                node=raw_input("Quel node supprimer ? (format NodeXCellX) : ")
                node=node.strip()
                myNode.supprimer()
            except ServException, e:
                logger.info(e.message)
        
        elif rep ==3: #pb statut
            try:
                node=raw_input("Quel node démarrer ? (format NodeXCellX) : ")
                node=node.strip()
                myNode.demarrer(node)
            except ServException, e:
                logger.info(e.message)

        elif rep == 4: #pb statut
            try:
                node=raw_input("Quel node arrêter ? (format NodeXCellX) : ")
                node=node.strip()
                myNode.arreter(node)
            except ServException, e:
                logger.info(e.message)

        elif rep == 5: 
            try:
                node=raw_input("Quel node consulter ? (format NodeX) : ")
                node=node.strip()
                myNode.statut(node)
            except ServException, e:
                logger.info(e.message)

        else:
            logger.info("Je n'ai pas compris.")


        retour = str(raw_input('Revenir au menu ? oui / non : ')) 