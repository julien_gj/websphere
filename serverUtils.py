# -*- coding:iso-8859-1 -*-

from org.apache.log4j import PropertyConfigurator
from org.apache.log4j import Logger

PropertyConfigurator.configure("log4j.properties")

import AdminServerManagement
import AdminApplication
import AdminNodeManagement
logger = Logger.getLogger("Serveur")


class Serveur:
    'serveur'
    def __init__(self, nom, nom_noeud):
        self.nom = nom
        self.nom_noeud = nom_noeud
        self.statut = "off"
        self.applications={}

    def __str__(self):
        return '[Serveur : %s, Noeud : %s, Statut : %s ]'%(self.nom,self.nom_noeud,self.statut)

    def demarrer(self):
        if self.statut == "off":
            # AdminControl.startServer(self.nom, self.nom_noeud)
            AdminServerManagement.startSingleServer(self.nom_noeud, self.nom)
            AdminConfig.save()
            logger.info("Server is now running")
            self.statut = "on"
        elif self.statut == "on":
            logger.warn("Serveur is already running")

    def arreter(self):
        if self.statut == "on":
            # AdminControl.stopServer(self.nom, self.nom_noeud)
            AdminServerManagement.stopSingleServer(self.nom_noeud, self.nom)
            AdminConfig.save()
            logger.info('Serveur %s is now off'%(self.nom)) # TODO ajouter le nom du serveur dans le message
            self.statut = "off"
        elif self.statut == "off":
            logger.warn('Serveur %s is already off'%(self.nom))

            
class Cellule:

    # Constructeur : une methode d'initialisation : def __init__
    # les methodes sont des fonctions qui travaillent sur un objet nomme par default "self"

    def __init__(self):
        self.serveurs = {} # tableau associatif { nom - <instance> }
        self.noeuds = []    # liste simple
    def __str__(self):
        string = self.afficherNoeuds()+'\n'+self.afficherServeurs()
        
    def synchroniser(self):
        #to-do : adapter avec la même fonction du code de Violette & Adrien
        'Recupere les informations des serveurs, construit la liste des serveurs et des nodes'
        infoServeurs = AdminTask.listServers('[-serverType APPLICATION_SERVER]') # et les nodeAgents ?
        listeServeur = infoServeurs.split("\n")
        for line in listeServeur:                    #on va decouper la sortie
            info = line.split("/")
            infoServeur = info[5].split("|")
            nomServeur = infoServeur[0]
            nomNoeud = info[3]                        
            serveur = Serveur(nomServeur, nomNoeud)    #instanciation du serveur
            self.serveurs[nomServeur]=serveur         #entree du tableau associatif
            if nomNoeud not in self.noeuds:         # remplissage de la liste de nodes
                self.noeuds.append(nomNoeud)
        return self.serveurs

    def creerServeur(self):
        nom = raw_input("Nom du serveur a creer :")
        nom_noeud = raw_input("Noeud ou installer le serveur ? :")
        serveur = Serveur(nom, nom_noeud)            #Instanciation d'un nouveau objet serveur
        AdminServerManagement.createApplicationServer(nom_noeud, nom, 'default')
        AdminConfig.save()
        logger.info("Serveur %s cree avec succes"%(nom))
        self.serveurs[nom]=serveur         #entree du tableau associatif

    def supprimerServeur(self):
        nom = raw_input("Nom du serveur a supprimer : ")
        nom_noeud = self.serveurs[nom].nom_noeud        #trouver le nom du noeud du serveur
        AdminServerManagement.deleteServer(nom_noeud, nom)
        AdminConfig.save()
        del self.serveurs[nom]
        logger.info("Serveur supprime") # TODO ajouter le nom du serveur supprime


    def installer(self,serveur, nodeName, serverName, contextRoot):    #TODO
        pass
        # AdminApplication.installWarFile(self, warFile, nodeName, serverName, contextRoot)


    def demarrerServeur(self):        #OK
        nom = raw_input("Nom du serveur a demarrer : ")
        serveur = self.serveurs[nom]        #trouver le nom du serveur
        serveur.demarrer()

    def arreterServeur(self):        #OK
        nom = raw_input("Nom du serveur a arreter : ")
        serveur = self.serveurs[nom]        #trouver le nom du serveur
        serveur.arreter()
        
    def afficherServeurs(self):                    #OK
        print("Liste des serveurs : ") 
        string = ''
        for serveur in self.serveurs.values():
            string += str(serveur)+'\n'
        print(string)
            
    def afficherNoeuds(self):                    #OK
        string = ''
        for noeud in self.noeuds :
            string += noeud+'\n'
        print(string)

if __name__ == '__main__':
	# util = Outils()
	util.synchroniser()
	util.afficherServeurs()
	# l.afficher()
	#l.demarrerServeur('server4')
	#l.afficher()
	#l.arreterServeur('server4')
	#l.afficher()
