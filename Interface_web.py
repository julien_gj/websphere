import AdminServerManagement
import AdminNodeManagement
import AdminApplication
# from subprocess import check_output
# import dao
#wsadmin  -wsadmin_classpath C:\IBM\WebSphere\AppServer\bin\log4j\log4j-1.2.14.jar
from org.apache.log4j import PropertyConfigurator
from org.apache.log4j import Logger
import os
import Application


#TODO : 
#Gestion Application
#Modelisation UML
#Gestion des exceptions
#Gestions traces/log
#Accès BDD


PropertyConfigurator.configure("C:\\Users\\ib\\Desktop\\Virtual_and_Windows\\Python_Hafid\\project\\log4j.properties")
logger = Logger.getLogger("Interface_Web")

class Error(Exception):
	"""Base class for other exceptions"""
	pass
class serverAlreadyExist(Error):
	"""Raised when server already exist"""
	pass
class wrongCombinaison(Error):
	"""Raised when server name and node name doesn't match"""
	pass
class serverAlreadyStopped(Error):
	"""Raised when server is already stopped"""
	pass
class serverAlreadyStarted(Error):
	"""Raised when server is already started"""
	pass
	
def getListServ():
	listed=AdminTask.listServers('[-serverType APPLICATION_SERVER]').split("\r\n")

	print "\n-----Server List-----\n"
	print '%-25s %-25s %-25s' % ("Nom", "Node","Cell")
	print "\n"
	
	for elements in listed:
		servName=elements.split("(")
		nodeName=elements.split("/")
		print '%-25s %-25s %-25s' % (servName[0], nodeName[3],nodeName[1])
		save_servers[servName[0]]=(nodeName[3],nodeName[1])

	return save_servers

def clear():
	pressToContinue()
	os.system("clear")
	presentation()


def pressToContinue():
	raw_input("\nPress Enter to continue :\n")

def serverStatus(name,node,cell):
	good=999
	if name in save_servers.keys(): 
		try :
			server = AdminControl.completeObjectName('cell='+cell+',node='+node+',name='+name+',type=Server,*')
			state = AdminControl.getAttribute(server, 'state')
			print "The server", name, "is", state
			logger.info(name + " is up and running!")
			good=0
			return good

		except:
			print "The server", name, "is STOPPED"
			logger.info(name + " is currently down!")
			good=1
			return good
	else :
		logger.Error("Serveur doesn't existe, you probably misspelled it")

def presentation():
	a="""
 __          __  _               _                     _____       _             __               
 \ \        / / | |             | |                   |_   _|     | |           / _|              
  \ \  /\  / /__| |__  ___ _ __ | |__   ___ _ __ ___    | |  _ __ | |_ ___ _ __| |_ __ _  ___ ___ 
   \ \/  \/ / _ \ '_ \/ __| '_ \| '_ \ / _ \ '__/ _ \   | | | '_ \| __/ _ \ '__|  _/ _` |/ __/ _ \\
	\  /\  /  __/ |_) \__ \ |_) | | | |  __/ | |  __/  _| |_| | | | ||  __/ |  | || (_| | (_|  __/
	 \/  \/ \___|_.__/|___/ .__/|_| |_|\___|_|  \___| |_____|_| |_|\__\___|_|  |_| \__,_|\___\___|
						  | |                                                                     
						  |_|                                                                     
	
	"""
	print(a)
	print("\n\n")

if __name__ == '__main__':

	save_servers={}

	os.system("clear")
	execution = 1
	while execution :
		os.system("clear")
		presentation()

		# getListServ()
		workingOnServer=0
		workingOnNode=0
		workingOnApp=0
		
		menuAns=raw_input("\nHello dear user, on what you wanna work ? (Servers, Nodes, Applications?)\n\nQ to quit\n\nInput : ")
		menuAns=menuAns.lower()
		if menuAns =="servers":
			workingOnServer=1
		if menuAns =="nodes":
			workingOnNode=1
		if menuAns =="applications" or menuAns =="app" :
			workingOnApp=1
		if menuAns =="q":
			execution=0
		if menuAns=="test":
			print save_servers
			for key in save_servers.keys():
				print("I'm ",key,"my node is",save_servers[key][0],"and my cell is ",save_servers[key][1])
			
			raw_input("\nPress Enter to continue :\n")
				
		while workingOnServer :
			clear()
			getListServ()
			print("\n-----Welcome to the servers menu-----")	
			ans = raw_input("\nWhat do you want to do my dear? \n\ncreate [ServerName] [NodeName], \ndelete [ServerName] [NodeName], \nstart [ServerName] [NodeName] [CellName], \nstop [ServerName] [NodeName] [CellName], \nstatus [ServerName] [NodeName] [CellName] \n\nR to return \nQ to quit\n\nInput : ")
			ans=ans.strip().split()	
			print("\n")
			ans[0]=ans[0].lower()
			
			try:
				if ans[0] == "create":
					try:
						print "\n-----Creating servers menu-----"
						print "Creating server : ", ans[1]
						if ans[1] not in save_servers:
							AdminServerManagement.createApplicationServer(ans[2], ans[1], "default")
							logger.info(ans[1]+" has successfully been created!")
						else :
							raise serverAlreadyExist		
					except IndexError:
						logger.warning("Number of parameters incorrect. Try again!")

					except serverAlreadyExist:
						logger.warning("Server already exists")

				if ans[0] == "delete":
					try:
						print "\n-----Deleting servers menu-----"
						print "Deleting server : ", ans[1]
						if ans[2] == save_servers[key][0]: 
							AdminServerManagement.deleteServer(ans[2], ans[1])
							logger.info(ans[1] + " has successfully been deleted!")
						else :
							raise wrongCombinaison	
					except wrongCombinaison :
						logger.warning("Wrong association of server name and node name")
						logger.warning("Did you mean the following node :",save_servers[key][0])
					except IndexError:
						logger.warning("Number of parameters incorrect. Try again!")

				if ans[0] == "start":
					try :
						if serverStatus(ans[1],ans[2],ans[3])==1:
							try:
								print "\n-----Starting servers menu-----"
								print "Starting server : ", ans[1]
								AdminServerManagement.startSingleServer(ans[2], ans[1])
								logger.info(ans[1] + " has successfully been started!")
							except IndexError:
								logger.warning("Number of parameters incorrect. Try again!")
						else :
							raise serverAlreadyStarted
					except serverAlreadyStarted:
						logger.warning("Server",ans[1],"is already started")

				if ans[0] == "stop":
					try :
						if serverStatus(ans[1],ans[2],ans[3])==0:
							try:
								print "\n-----Stoping servers menu-----"
								print "Stoping server : ",ans[1]
								AdminServerManagement.stopSingleServer(ans[2],ans[1])
								logger.info(ans[1] + " has successfully been stopped!")
							except IndexError:
								logger.warning("Number of parameters incorrect. Try again!")

						else :
							raise serverAlreadyStopped
					except serverAlreadyStopped:
						logger.warning("Server",ans[1],"is already stopped")
				
				if ans[0] == "status":
					print "\n-----Status servers menu-----"
					#mettre try execpt dans def

					#Gerer erreur sui renvoit rien :

					#check if server in the list, alors server stop
					#check is server in the list, if not then wrong serverName

					try:
						serverStatus(ans[1],ans[2],ans[3])
					except:
						print "The server", ans[1], "is STOPPED"
						logger.info(ans[1] + " is currently down!")

				if ans[0]=="r" or ans[0]=="return":
					workingOnServer=0

				if ans[0] == "quit" or ans[0] == "q":
					logger.info("The application has been successfully been closed!")
					execution = 0
					workingOnServer=0
			except IndexError:
				logger.warning("No command has been specified. Try again!")
		
		while workingOnNode :
			clear()
			print("\n-----Welcome to the Nodes menu-----")	
			os.system("C:/IBM/WebSphere/AppServer/bin/manageprofiles.bat -listProfiles")
			print("-------------------------------------")

			ans = raw_input("\nWhat do you want to do my dear? \n\ncreate [profileName] [NodeName] [cellName] \ndelete [profileName] \nstart [NodeProfileName] \nstop [NodeProfileName] \nstatus [NodeName]\n\nR to return \nQ to quit\n\nInput : ")
			print("\n")
			ans=ans.strip().split()	
			ans[0]=ans[0].lower()
			try :
				if ans[0] == "create":
					#check is node already exist
					cmd="C:/IBM/WebSphere/AppServer/bin/manageprofiles.bat -create -templatePath C:/IBM/WebSphere/AppServer/profileTemplates/managed -profileName %s -profilePath C:/IBM/WebSphere/AppServer/profiles/%s -hostname WINIB-E6B6L3MGG -nodeName %s -cellName %s -dmgrHost WINIB-E6B6L3MGG -dmgrPort 8879 -dmgrAdminUserName websphere -dmgrAdminPassword websphere" %(ans[1],ans[1],ans[2],ans[3])					
					os.system(cmd)
				if ans[0]=="delete":
					#check is node in the list
					cmd="C:/IBM/WebSphere/AppServer/bin/manageprofiles.bat -delete -profileName %s -username websphere -password websphere" %(ans[1])
					os.system(cmd)
				if ans[0]=="start":
					#check if node in the list and status before
					cmd="C:/IBM/WebSphere/AppServer/bin/startNode.bat -profileName %s -username websphere -password websphere" %(ans[1])
					os.system(cmd)
				if ans[0]=="stop":
					#check if node in the list and status before

					cmd="C:/IBM/WebSphere/AppServer/bin/stopNode.bat -profileName %s -username websphere -password websphere" %(ans[1])
					os.system(cmd)
				if ans[0]=="status":
					#check if node in list
					if AdminNodeManagement.isNodeRunning(ans[1])==1:
						print "Your node ",ans[1]," is running"
					else :
						print "Your node ",ans[1]," is not running"

				if ans[0]=="r" or ans[0]=="return":
					workingOnNode=0

				if ans[0] == "quit" or ans[0] == "q":
					logger.info("The application has been successfully been closed!")
					execution = 0
					workingOnNode=0
				if ans[0]=="cheat":
					AdminNodeManagement.listNodes()
			except IndexError:
				logger.warning("No command has been specified. Try again!")
		
		while workingOnApp :
			try:
				clear()
				print("\n-----Welcome to the Application menu-----")	
				print("-------------------------------------")
				getListServ()
				Application.getListApp()
				rep = raw_input("\nWhat do you want to do ? \n - install - an application [ServerName] [NodeName] \n - uninstall - an application [ServerName] [NodeName] \n - start - an application [ServerName] [NodeName] \n - stop - an application [ServerName] [NodeName] \n - status - check the status of an application \n\n - Q - to quit\n - R - to return \nInput: ")
				print "\n"
				rep = rep.strip().split()
				
				if rep[0] == "install" :
					try:
						print "\n-----Installing Application menu-----"
						Application.installApp()
					except IndexError:
						logger.warning("Number of parameters incorrect. Try again!")

				if rep[0] == "uninstall":
					try:
						print "\n-----Uninstalling Application menu-----"
						Application.uninstall()
					except IndexError:
						logger.warning("Number of parameters incorrect. Try again!")

				if rep[0] == "start":
					try:
						print "\n-----Starting Application menu-----"
						Application.start()
					except IndexError:
						logger.warning("Number of parameters incorrect. Try again!")

				if rep[0] == "stop":
					try:
						print "\n-----Stopping Application menu-----"
						Application.stop()
					except IndexError:
						logger.warning("Number of parameters incorrect. Try again!")
				if rep[0] == "status":
					try:
						print "\n-----Application Status menu-----"
						Application.status()
					except IndexError:
						logger.warning("Number of parameters incorrect. Try again!")
				if rep[0]=="r" or rep[0]=="return":
					workingOnApp=0

				if rep[0] == "Q" or rep[0] == "quit" or rep[0] == "q":
					logger.info("The application has been successfully been closed!")
					execution = 0
					workingOnApp=0
			except IndexError:
				logger.warning("No command has been specified. Try again!")
			
