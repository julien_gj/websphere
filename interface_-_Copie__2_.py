import AdminServerManagement
import AdminNodeManagement
import AdminApplication

from org.apache.log4j import PropertyConfigurator
from org.apache.log4j import Logger
import os
#coding: iso8859_1
from serverUtils import *
# import Node
from cellUtils import *
from Interface_web_vio import *
from Node import *

PropertyConfigurator.configure("C:\\IBMWebSphere\\AppServer\\scripts\\websphere\\websphere\\log4j.properties")
logger = Logger.getLogger("Interface_Web")

class ServException(Exception):
	def __init__(self, message):
		self.message = message

def operations():
	try:
		rep = "null"
		while  rep != "0" :
			print("***************************")
			print("*********** MENU **********")
			print("***************************")
			rep = raw_input("Gestion des noeuds       :1\nGestion des serveurs     :2\nGestion des applications :3\nGestion des Clusters     :4\n"
						"Gestion des Cellules     :5\nFin des operations       :0\nVotre choix:============>:")
			if rep == "1":
				nodeGestion()
			elif rep == "2":
				serversGestion()
			elif rep == "3":
				appGestion()
			elif rep == "4":
				clustersGestion()
			elif rep == "5":
				cellGestion()
	except:
		operations()

def nodeGestion():

	try:
		rep = "null"

		while  rep != "0" :
			myNode=Node("commetuveux")
			myList=listNodes()
			save_servers={}
			
			cellUtils.getListServ()

			print("**************************")
			print("*** GESTION DES NOEUDS ***")
			print("**************************")
			rep = raw_input("Creer un noeud          :1\nDemarrer un Noeud       :2\nSupprimer un Noeud      :3\nArreter un noeud        :4\n"
						"Statut des noeuds       :5\nAfficher les noeuds     :6\nFin des operations      :0\nVotre choix:===========>:")
			if rep == "1":
				myList.creer()

			if rep == "2":#pb statut serveur 'off'
				try:
					node=raw_input("Quel node démarrer ? (format NodeXCellX) : ")
					node=node.strip()
					myNode.demarrer(node)
				except ServException, e:
					logger.info(e.message)

			elif rep == "3": 
				try:
					node=raw_input("Quel node supprimer ? (format NodeXCellX) : ")
					node=node.strip()
					myNode.supprimer()
				except ServException, e:
					logger.info(e.message)

			elif rep == "4":#pb statut serveur 'off'
				try:
					node=raw_input("Quel node arrêter ? (format NodeXCellX) : ")
					node=node.strip()
					myNode.arreter(node)
				except ServException, e:
					logger.info(e.message)

			elif rep == "5":
				try:
					node=raw_input("Quel node arrêter ? (format NodeXCellX) : ")
					node=node.strip()
					myNode.arreter(node)
				except ServException, e:
					logger.info(e.message)
				
			elif rep == "6":
				print("Ce n'est pas développé...")
				pass

	except:
		nodeGestion()


def appGestion():
	try:
		rep = "null"

		while  rep != "0" :
			print("********************************")
			print("*** GESTION DES APPLICATIONS ***")
			print("********************************")
			rep = input("Installer une Application     :1\ndesinstaller une Application  :2\nDemarrer une Application      :3\n"
						"Arreter une Application       :4\nListe des Application         :5\nStatut                        :6\n"
						"Terminer les operations       :0\nVotre choix:================>:")
			if rep == "1":
				installApp()
			elif rep == "2":
				uninstallApp()
			if rep == "3":
				startApp()
			if rep == "4":
				stopApp()
			if rep == "5":
				getlistApp()
			if rep == "6":
				status()
	except:
		appGestion()

def clustersGestion():
	try:
		rep = "null"
		while  rep != "0" :
			print("****************************")
			print("*** GESTION DES Clusters ***")
			print("****************************")
			rep = input("Creer un Cluster          :1\nSuprimer un Cluster       :2\nListe des Clusters        :3\n"
						"Etat des Clusters         :4\nEtat des Serveurs         :5\nArreter Cluster           :6\n"
						"demarrerCluster()         :7\nRetour au Menu            :0\nVotre choix:=============>:")
			if rep == "1":
				creerGrappe()
			elif rep == "2":
				supprimerGrappe()
			if rep == "3":
				inventaireClusters()
			if rep == "4":
				clusterStatus()
			if rep == "5":
				arreterCluster()
			if rep == "6":
				demarrerCluster()

	except:
		clustersGestion()

def serversGestion():
	myCell= serverUtils.Cellule()
	myCell.synchroniser()
	try:
		rep = "null"
		
		while  rep != "0" :
			print ("************************")
			print ("**GESTION DES SERVEURS**")
			print ("************************")
			rep = input("Creer un Serveur      :1\nSupprimer un Serveur  :2\nDemarrer un serveur   :3\nAfficher les serveurs :4\n"
						"Fin des operations    :0\nVotre choix:=========>:")
			if rep == "1":
				myCell.creerServeur()			
			elif rep == "2":
				myCell.supprimerServeur()
			elif rep == "3":
				myCell.demarrerServeur()
			elif rep == "4":
				myCell.afficherServeurs()

	except:
		serversGestion()
def cellGestion():
	try:
		rep = "null"
		while  rep != "0" :
			print("****************************")
			print("*** GESTION DES Cellules ***")
			print("****************************")
			rep = raw_input("Creer une Cellule         :1\nSuprimer une Cellule      :2\nDemarrer une Cellule      :3\nArreter une Cellule       :4\n"
						"Retour au Menu            :0\nVotre choix:=============>:")
			if rep == "1":
				ans = raw_input("\ncreate [profileName] [cellName]\n\nInput : ")
				ans=ans.strip().split()	
				ans[0]=ans[0].lower()
				cellUtils.create_cell(ans[1],ans[2])
			if rep == "2":
				ans = raw_input("\ndelete [profileName] \n\nInput : ")
				ans=ans.strip().split()	
				ans[0]=ans[0].lower()				
				cellUtils.delete_cell(ans[1])
			if rep == "3":
				ans = raw_input("\nstart [profileName] \n\nInput : ")
				ans=ans.strip().split()	
				ans[0]=ans[0].lower()				
				cellUtils.start_cell(ans[1])
			if rep == "4":
				ans = raw_input("\nstop [profileName] \n\nInput : ")
				ans=ans.strip().split()	
				ans[0]=ans[0].lower()				
				cellUtils.stop_cell(ans[1])
	except:
		cellGestion()	



operations()